package accessors

import (
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/pdk"
)

type ServiceAccessor struct {
	TraceId     string
	Logger      facade.LogEvent
	Context     pdk.Context
	Connector   pdk.Connector
	ServiceNode pdk.ServiceNode
	Config      facade.YamlParser
	Handler     facade.Handler
	Version     string
}
